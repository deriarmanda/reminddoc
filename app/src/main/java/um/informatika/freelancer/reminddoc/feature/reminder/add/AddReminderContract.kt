package um.informatika.freelancer.reminddoc.feature.reminder.add

import um.informatika.freelancer.reminddoc.base.BasePresenter
import um.informatika.freelancer.reminddoc.base.BaseView
import um.informatika.freelancer.reminddoc.data.model.Reminder

interface AddReminderContract {

    interface Presenter : BasePresenter {
        fun loadDokterList()
        fun saveReminder(reminder: Reminder)
        fun updateReminder(reminder: Reminder)
    }

    interface View : BaseView<Presenter> {
        fun onLoadDokterListSuccess(list: Collection<String>)
        fun onLoadDokterListError(message: String)
        fun onSaveReminderSuccess()
        fun onSaveReminderError(message: String)
    }
}