package um.informatika.freelancer.reminddoc.util

import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity

const val FRAGMENT_TAG_HOME = "tag_home"
const val FRAGMENT_TAG_REMINDER = "tag_reminder"
const val FRAGMENT_TAG_REPORT = "tag_report"
const val FRAGMENT_TAG_RATING = "tag_rating"

fun AppCompatActivity.replaceFragmentByTag(@IdRes containerId: Int, fragment: Fragment, tag: String) {
    supportFragmentManager.beginTransaction()
        .replace(containerId, fragment, tag)
        .commit()
}