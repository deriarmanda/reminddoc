package um.informatika.freelancer.reminddoc.feature.login

import um.informatika.freelancer.reminddoc.base.BasePresenter
import um.informatika.freelancer.reminddoc.base.BaseView

interface LoginContract {

    interface Presenter : BasePresenter {
        fun loginToFirebase(email: String, password: String)
        fun readUserInfo()
    }

    interface View : BaseView<Presenter> {
        fun onLoginSuccess(name: String)
        fun onLoginError(message: String)
    }
}