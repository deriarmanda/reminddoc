package um.informatika.freelancer.reminddoc.base

interface BaseView<T : BasePresenter> {
    val presenter: T
    fun showLoadingIndicator(isActive: Boolean)
}