package um.informatika.freelancer.reminddoc.feature.report

import um.informatika.freelancer.reminddoc.base.BasePresenter
import um.informatika.freelancer.reminddoc.base.BaseView
import um.informatika.freelancer.reminddoc.data.model.Reminder

interface ReportContract {

    interface Presenter : BasePresenter {
        val laporanBulanan: Map<String, List<Reminder>>
        val laporanHarian: Map<String, List<Reminder>>

        fun breakOperations()
        fun loadAllLaporanBulanan()
        fun loadAllLaporanHarian()
        fun loadSelectedLaporanBulanan(bulan: String)
        fun loadSelectedLaporanHarian(hari: String)
        fun openDetailPage(reminder: Reminder)
    }

    interface View : BaseView<Presenter> {
        fun showLaporanKeyList(list: List<String>)
        fun showSelectedLaporanList(list: List<Reminder>)
        fun onLoadAllLaporanFailed(message: String)
        fun showDetailPage(reminder: Reminder)
    }
}