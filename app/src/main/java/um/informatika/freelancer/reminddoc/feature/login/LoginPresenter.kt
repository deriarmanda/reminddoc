package um.informatika.freelancer.reminddoc.feature.login

import com.google.firebase.auth.FirebaseAuth
import um.informatika.freelancer.reminddoc.base.BaseCallback
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager

class LoginPresenter(
    private val view: LoginContract.View,
    private val accountManager: AccountDataManager
) : LoginContract.Presenter {

    private val mAuth = FirebaseAuth.getInstance()

    override fun loginToFirebase(email: String, password: String) {
        if (email.isBlank() || password.isBlank()) view.onLoginError("Kolom tidak boleh kosong !")
        else {
            view.showLoadingIndicator(true)
            mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener {
                if (it.isSuccessful) readUserInfo()
                else {
                    view.showLoadingIndicator(false)
                    view.onLoginError("Terjadi kesalahan, silahkan periksa akun anda serta koneksi internet anda.")
                }
            }
        }
    }

    override fun readUserInfo() {
        accountManager.checkAndPushFirebaseToken(
            object : BaseCallback<Unit> {
                override fun onSuccess(data: Unit) {
                    accountManager.loadUserInfoFromDatabase(
                        object : BaseCallback<String> {
                            override fun onSuccess(data: String) {
                                view.showLoadingIndicator(false)
                                view.onLoginSuccess(data)
                            }

                            override fun onError(message: String) {
                                view.showLoadingIndicator(false)
                                view.onLoginError(message)
                            }
                        }
                    )
                }

                override fun onError(message: String) {
                    view.showLoadingIndicator(false)
                    view.onLoginError(message)
                }
            }
        )
    }

    override fun start() {}
}