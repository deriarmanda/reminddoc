package um.informatika.freelancer.reminddoc.feature.reminder

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_reminder.view.*
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.util.DateTimeUtils

class ReminderRvAdapter(
    private val buttonMoreClickedListener: OnButtonMoreClickedListener
) : RecyclerView.Adapter<ReminderRvAdapter.ViewHolder>() {

    var reminderList = emptyList<Reminder>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_reminder, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = reminderList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(reminderList[position], position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch (reminder: Reminder, position: Int) {
            itemView.text_date.text = DateTimeUtils.getHumanDateTimeString(reminder.tanggal)
            itemView.text_doctor.text = reminder.namaDokter
            itemView.text_patient.text = String.format("%s / %s", reminder.nomorRM, reminder.namaPasien)
            itemView.button_more.setOnClickListener {
                buttonMoreClickedListener.onClick(reminder)
            }

            if (position%2 == 0) {
                itemView.setBackgroundResource(R.color.solid_white)
                itemView.divider_top.visibility = View.INVISIBLE
                itemView.divider_bottom.visibility = View.INVISIBLE
            } else {
                itemView.setBackgroundResource(R.color.plain)
                itemView.divider_top.visibility = View.VISIBLE
                itemView.divider_bottom.visibility = View.VISIBLE
            }
        }
    }

    interface OnButtonMoreClickedListener {
        fun onClick(reminder: Reminder)
    }
}