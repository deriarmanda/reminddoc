package um.informatika.freelancer.reminddoc.feature.reminder


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import kotlinx.android.synthetic.main.fragment_reminder.*
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.manager.ReminderDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.feature.reminder.add.AddReminderActivity
import um.informatika.freelancer.reminddoc.feature.reminder.detail.DetailReminderActivity
import um.informatika.freelancer.reminddoc.util.showErrorMessage

/**
 * A simple [Fragment] subclass.
 *
 */
class ReminderFragment : Fragment(), ReminderContract.View {

    override lateinit var presenter: ReminderContract.Presenter
    private val buttonMoreClickedListener = object : ReminderRvAdapter.OnButtonMoreClickedListener {
        override fun onClick(reminder: Reminder) {
            presenter.openDetailPage(reminder)
        }
    }
    private val rvAdapter = ReminderRvAdapter(buttonMoreClickedListener)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_reminder, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(refresh_layout) {
            setColorSchemeResources(
                R.color.accent
            )
            setOnRefreshListener {
                spinner_sorting.setSelection(0)
                presenter.loadReminderList()
            }
        }
        with(list_reminder) {
            layoutManager = LinearLayoutManager(context)
            adapter = rvAdapter
        }
        spinner_sorting.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                presenter.sortList(spinner_sorting.selectedItem.toString())
            }
        }
        fab_add.setOnClickListener { startActivity(AddReminderActivity.getIntent(context!!)) }

        presenter = ReminderPresenter(
            this,
            AccountDataManager.getInstance(),
            ReminderDataManager.getInstance()
        )
    }

    override fun onStart() {
        super.onStart()
        presenter.start()
    }

    override fun onPause() {
        super.onPause()
        presenter.breakOperations()
    }

    override fun showAddButton() = fab_add.show()

    override fun hideAddButton() = fab_add.hide()

    override fun showReminderList(list: List<Reminder>) {
        rvAdapter.reminderList = list
        text_empty.visibility = View.GONE
        list_reminder.visibility = View.VISIBLE
    }

    override fun showEmptyListMessage() {
        text_empty.visibility = View.VISIBLE
        list_reminder.visibility = View.GONE
    }

    override fun onLoadReminderListFailed(message: String) {
        (activity as AppCompatActivity).showErrorMessage(message)
    }

    override fun showDetailPage(reminder: Reminder) {
        startActivity(DetailReminderActivity.getIntent(context!!, reminder))
    }

    override fun showLoadingIndicator(isActive: Boolean) {
        refresh_layout.isRefreshing = isActive
        spinner_sorting.isEnabled = !isActive
    }

    fun searchList(query: String) {
        presenter.searchList(query)
    }
}
