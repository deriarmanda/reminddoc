package um.informatika.freelancer.reminddoc.base

interface BasePresenter {
    fun start()
}