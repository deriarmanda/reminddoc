package um.informatika.freelancer.reminddoc.feature.splash

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_splash.*
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.base.BaseCallback
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.feature.login.LoginActivity
import um.informatika.freelancer.reminddoc.feature.main.MainActivity
import um.informatika.freelancer.reminddoc.util.showInfoMessage

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Toasty.Config.getInstance()
            .setTextSize(12)
//            .setSuccessColor(0x03A9F4)
//            .setErrorColor(0xC2185B)
            .apply()

        val accountManager = AccountDataManager.getInstance()
        val isOpenedFromNotif = intent.hasExtra("uid")
        val loadInfoCallback = object : BaseCallback<String> {
            override fun onSuccess(data: String) {
                this@SplashActivity.showInfoMessage("Selamat datang kembali, $data")
                if (isOpenedFromNotif) {
                    val reminder = Reminder(
                        intent.getStringExtra("tanggal"),
                        intent.getStringExtra("nomorRM"),
                        intent.getStringExtra("namaPasien"),
                        intent.getStringExtra("keterangan"),
                        intent.getStringExtra("status"),
                        intent.getStringExtra("uidDokter"),
                        intent.getStringExtra("uidPetugas"),
                        intent.getStringExtra("uid"),
                        intent.getStringExtra("namaPetugas"),
                        intent.getStringExtra("namaDokter")
                    )
                    startActivity(MainActivity.getIntent(this@SplashActivity, reminder))
                } else startActivity(MainActivity.getIntent(this@SplashActivity))
                finish()
            }

            override fun onError(message: String) {
                progress_bar.visibility = View.GONE
                button_retry.visibility = View.VISIBLE
                text_error.visibility = View.VISIBLE
            }
        }
        val checkTokenCallback = object : BaseCallback<Unit> {
            override fun onSuccess(data: Unit) = accountManager.loadUserInfoFromDatabase(loadInfoCallback)
            override fun onError(message: String) {
                progress_bar.visibility = View.GONE
                button_retry.visibility = View.VISIBLE
                text_error.visibility = View.VISIBLE
            }
        }

        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {
            startActivity(LoginActivity.getIntent(this))
            finish()
        } else accountManager.checkAndPushFirebaseToken(checkTokenCallback)

        button_retry.setOnClickListener {
            progress_bar.visibility = View.VISIBLE
            button_retry.visibility = View.GONE
            text_error.visibility = View.GONE
            accountManager.checkAndPushFirebaseToken(checkTokenCallback)
        }
    }
}
