package um.informatika.freelancer.reminddoc.data.manager

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import um.informatika.freelancer.reminddoc.base.BaseCallback
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.util.DateTimeUtils

class ReminderDataManager private constructor() {

    private val tag = ReminderDataManager::class.java.canonicalName
    private val dbRef = FirebaseDatabase.getInstance().reference
    private val userNameList = hashMapOf<String, String>()

    companion object {
        private var sInstance: ReminderDataManager? = null
        fun getInstance(): ReminderDataManager {
            if (sInstance == null) sInstance = ReminderDataManager()
            return sInstance!!
        }
    }

    fun insertReminder(reminder: Reminder, callback: BaseCallback<Unit>) {
        val reminderRef = dbRef.child("reminders")
        reminderRef.push()
            .setValue(reminder) { dbError, _ ->
                if (dbError != null) callback.onError(dbError.message)
                else callback.onSuccess(Unit)
            }
    }

    fun updateReminder(reminder: Reminder, callback: BaseCallback<Unit>) {
        val reminderRef = dbRef.child("reminders").child(reminder.uid)
        reminderRef.updateChildren(
            mapOf(
                Pair("/nomorRM", reminder.nomorRM),
                Pair("/namaPasien", reminder.namaPasien),
                Pair("/tanggal", reminder.tanggal),
                Pair("/keterangan", reminder.keterangan),
                Pair("/status", reminder.status),
                Pair("/uidDokter", reminder.uidDokter),
                Pair("/uidPetugas", reminder.uidPetugas)
            )
        ) { dbError, _ ->
            if (dbError != null) callback.onError(dbError.message)
            else callback.onSuccess(Unit)
        }
    }

    fun updateStatusReminder(reminder: Reminder, callback: BaseCallback<Unit>) {
        val reminderRef = dbRef.child("reminders").child(reminder.uid)
        reminderRef.updateChildren(
            mapOf(
                Pair("/status", reminder.status)
            )
        ) { dbError, _ ->
            if (dbError != null) callback.onError(dbError.message)
            else callback.onSuccess(Unit)
        }
    }

    fun loadReminderList(uid: String, role: String, callback: BaseCallback<List<Reminder>>) {
        val remindersRef = dbRef.child("reminders")
        val query =
            if (role == "petugas") remindersRef.orderByChild("uidPetugas")
            else remindersRef.orderByChild("uidDokter")

        query.equalTo(uid).addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(dbError.message)
                    Log.d(tag, dbError.details)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val list = arrayListOf<Reminder>()
                        for (data in snapshot.children) {
                            val reminder = data.getValue(Reminder::class.java)
                            if (reminder != null) {
                                reminder.uid = data.key ?: ""
                                findUserName(
                                    reminder.uidPetugas,
                                    object : BaseCallback<String> {
                                        override fun onSuccess(data: String) {
                                            reminder.namaPetugas = data
                                        }

                                        override fun onError(message: String) {
                                            Log.d(tag, message)
                                        }
                                    }
                                )
                                findUserName(
                                    reminder.uidDokter,
                                    object : BaseCallback<String> {
                                        override fun onSuccess(data: String) {
                                            reminder.namaDokter = data
                                        }

                                        override fun onError(message: String) {
                                            Log.d(tag, message)
                                        }
                                    }
                                )
                                if (reminder.status == "Belum Lengkap") list.add(reminder)
                            } else {
                                callback.onError("Terjadi kesalahan ketika menerima data pengingat, silahkan coba lagi.")
                                Log.d(
                                    tag,
                                    "NullPointer occured while trying to parse Reminder Class in loadReminderList function. UID: ${data.key}"
                                )
                                return
                            }
                        }
                        callback.onSuccess(list)
                    } else callback.onSuccess(emptyList())
                }
            }
        )
    }

    fun loadDokterList(callback: BaseCallback<Map<String, String>>) {
        val usersRef = dbRef.child("users").orderByChild("role").equalTo("dokter")
        usersRef.addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(dbError.message)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val map = hashMapOf<String, String>()
                        for (data in snapshot.children) {
                            val uid = data.key ?: ""
                            val name = data.child("name").getValue(String::class.java) ?: ""
                            map[uid] = name
                        }
                        callback.onSuccess(map)
                    } else callback.onSuccess(emptyMap())
                }
            }
        )
    }

    fun loadLaporanBulananList(uid: String, role: String, callback: BaseCallback<Map<String, List<Reminder>>>) {
        val remindersRef = dbRef.child("reminders")
        val query =
            if (role == "petugas") remindersRef.orderByChild("uidPetugas")
            else remindersRef.orderByChild("uidDokter")

        query.equalTo(uid).addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(dbError.message)
                    Log.d(tag, dbError.details)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val map = hashMapOf<String, ArrayList<Reminder>>()
                        for (data in snapshot.children) {
                            val reminder = data.getValue(Reminder::class.java)
                            if (reminder != null) {
                                reminder.uid = data.key ?: ""
                                findUserName(
                                    reminder.uidPetugas,
                                    object : BaseCallback<String> {
                                        override fun onSuccess(data: String) {
                                            reminder.namaPetugas = data
                                        }

                                        override fun onError(message: String) {
                                            Log.d(tag, message)
                                        }
                                    }
                                )
                                findUserName(
                                    reminder.uidDokter,
                                    object : BaseCallback<String> {
                                        override fun onSuccess(data: String) {
                                            reminder.namaDokter = data
                                        }

                                        override fun onError(message: String) {
                                            Log.d(tag, message)
                                        }
                                    }
                                )
                                val key = DateTimeUtils.getHumanMonthYearOnly(reminder.tanggal)
                                if (map.contains(key)) map[key]?.add(reminder)
                                else map[key] = arrayListOf(reminder)
                            } else {
                                callback.onError("Terjadi kesalahan ketika menerima data pengingat, silahkan coba lagi.")
                                Log.d(
                                    tag,
                                    "NullPointer occured while trying to parse Reminder Class in loadReminderList function. UID: ${data.key}"
                                )
                                return
                            }
                        }
                        for (list in map.values) list.sortByDescending { it.tanggal }
                        callback.onSuccess(map)
                    } else callback.onSuccess(emptyMap())
                }
            }
        )
    }

    fun loadLaporanHarianList(uid: String, role: String, callback: BaseCallback<Map<String, List<Reminder>>>) {
        val remindersRef = dbRef.child("reminders")
        val query =
            if (role == "petugas") remindersRef.orderByChild("uidPetugas")
            else remindersRef.orderByChild("uidDokter")

        query.equalTo(uid).addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) {
                    callback.onError(dbError.message)
                    Log.d(tag, dbError.details)
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val map = hashMapOf<String, ArrayList<Reminder>>()
                        for (data in snapshot.children) {
                            val reminder = data.getValue(Reminder::class.java)
                            if (reminder != null) {
                                reminder.uid = data.key ?: ""
                                findUserName(
                                    reminder.uidPetugas,
                                    object : BaseCallback<String> {
                                        override fun onSuccess(data: String) {
                                            reminder.namaPetugas = data
                                        }

                                        override fun onError(message: String) {
                                            Log.d(tag, message)
                                        }
                                    }
                                )
                                findUserName(
                                    reminder.uidDokter,
                                    object : BaseCallback<String> {
                                        override fun onSuccess(data: String) {
                                            reminder.namaDokter = data
                                        }

                                        override fun onError(message: String) {
                                            Log.d(tag, message)
                                        }
                                    }
                                )
                                val key = DateTimeUtils.getHumanDateString(reminder.tanggal)
                                if (map.contains(key)) map[key]?.add(reminder)
                                else map[key] = arrayListOf(reminder)
                            } else {
                                callback.onError("Terjadi kesalahan ketika menerima data pengingat, silahkan coba lagi.")
                                Log.d(
                                    tag,
                                    "NullPointer occured while trying to parse Reminder Class in loadReminderList function. UID: ${data.key}"
                                )
                                return
                            }
                        }
                        for (list in map.values) list.sortByDescending { it.tanggal }
                        callback.onSuccess(map)
                    } else callback.onSuccess(emptyMap())
                }
            }
        )
    }

    private fun findUserName(uid: String, callback: BaseCallback<String>) {
        if (userNameList.contains(uid)) callback.onSuccess(userNameList[uid]!!)
        else {
            val usersRef = dbRef.child("users").child(uid)
            usersRef.addListenerForSingleValueEvent(
                object : ValueEventListener {
                    override fun onCancelled(dbError: DatabaseError) {
                        callback.onError(dbError.details)
                    }

                    override fun onDataChange(data: DataSnapshot) {
                        val name = data.child("name").getValue(String::class.java) ?: ""
                        userNameList[uid] = name
                        callback.onSuccess(name)
                    }
                }
            )
        }
    }
}