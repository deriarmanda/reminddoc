package um.informatika.freelancer.reminddoc.data.manager

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.iid.FirebaseInstanceId
import um.informatika.freelancer.reminddoc.base.BaseCallback

class AccountDataManager private constructor() {

    private val mAuth: FirebaseAuth = FirebaseAuth.getInstance()
    private val mDatabase = FirebaseDatabase.getInstance().getReference("/users")
    var uid: String = ""
    var name: String = ""
    var role: String = "dokter"
    var rate: Int = 0

    companion object {
        private var sInstance: AccountDataManager? = null
        fun getInstance(): AccountDataManager {
            if (sInstance == null) sInstance = AccountDataManager()
            return sInstance!!
        }
    }

    fun doLogout() {
        mAuth.signOut()
        uid = ""
        name = ""
        role = "dokter"
        rate = 0
    }

    fun loadUserInfoFromDatabase(callback: BaseCallback<String>) {
        val eventListener = object : ValueEventListener {
            override fun onCancelled(dbError: DatabaseError) = callback.onError(dbError.message)
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    name = snapshot.child("name").getValue(String::class.java) ?: ""
                    role = snapshot.child("role").getValue(String::class.java) ?: "dokter"
                    rate = snapshot.child("rate").getValue(Int::class.java) ?: 0
                    callback.onSuccess(name)
                } else callback.onError("Akun anda belum terverifikasi. Silahkan hubungi petugas untuk melakukan proses verifikasi akun.")
            }
        }
        val dbRef = mDatabase.child(uid)
        dbRef.addListenerForSingleValueEvent(eventListener)
    }

    fun checkAndPushFirebaseToken(callback: BaseCallback<Unit>) {
        uid = mAuth.currentUser?.uid ?: "-"
        val dbRef = mDatabase.child(uid)
        dbRef.child("token").addListenerForSingleValueEvent(
            object : ValueEventListener {
                override fun onCancelled(dbError: DatabaseError) = callback.onError(dbError.message)

                override fun onDataChange(snapshot: DataSnapshot) {
                    pushFirebaseToken(callback)
                }
            }
        )
    }

    fun sendUserRating(rate: Int, callback: BaseCallback<Unit>) {
        val dbRef = mDatabase.child(uid)
        dbRef.child("rate").setValue(rate) { dbError, _ ->
            if (dbError != null) callback.onError(dbError.message)
            else {
                this.rate = rate
                callback.onSuccess(Unit)
            }
        }
    }

    private fun pushFirebaseToken(callback: BaseCallback<Unit>) {
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener {
            if (it.isSuccessful) {
                val dbRef = mDatabase.child(uid)
                val token = it.result?.token
                Log.d("AccountDataManager", "push Token: $token")
                dbRef.child("token").setValue(token) { dbError, _ ->
                    if (dbError != null) callback.onError(dbError.message)
                    else callback.onSuccess(Unit)
                }
            } else callback.onError(it.exception?.message ?: "Terjadi kesalahan ketika memeriksa token aplikasi.")
        }
    }

}