package um.informatika.freelancer.reminddoc.feature.reminder

import um.informatika.freelancer.reminddoc.base.BaseCallback
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.manager.ReminderDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder

class ReminderPresenter(
    private val view: ReminderContract.View,
    private val accountManager: AccountDataManager,
    private val reminderManager: ReminderDataManager
) : ReminderContract.Presenter {

    override var isRunning: Boolean = false

    override fun breakOperations() {
        isRunning = false
    }

    private lateinit var usersRole: String
    private val reminderList = arrayListOf<Reminder>()

    override fun checksUserRole() {
        usersRole = accountManager.role
        if (usersRole == "petugas") view.showAddButton()
        else view.hideAddButton()
    }

    override fun loadReminderList() {
        view.showLoadingIndicator(true)
        reminderManager.loadReminderList(
            accountManager.uid,
            usersRole,
            object : BaseCallback<List<Reminder>> {
                override fun onSuccess(data: List<Reminder>) {
                    if (!isRunning) return
                    reminderList.clear()
                    reminderList.addAll(data)
                    sortList("Tanggal Terbaru")
                }

                override fun onError(message: String) {
                    if (!isRunning) return
                    view.showLoadingIndicator(false)
                    view.onLoadReminderListFailed(message)
                }
            }
        )
    }

    override fun sortList(sortBy: String) {
        when (sortBy) {
            "Dokter Tujuan" -> reminderList.sortWith(compareBy { it.namaDokter })
            "Pasien" -> reminderList.sortWith(compareBy { it.namaPasien })
            else -> reminderList.sortByDescending { it.tanggal }
        }
        view.showLoadingIndicator(false)
        if (reminderList.isEmpty()) view.showEmptyListMessage()
        else view.showReminderList(reminderList)
    }

    override fun searchList(query: String) {
        val filtered = reminderList.filter {
            it.nomorRM.contains(query) ||
                    it.namaPasien.contains(query) ||
                    it.namaDokter.contains(query)
        }
        if (filtered.isEmpty()) view.showEmptyListMessage()
        else view.showReminderList(filtered)
    }

    override fun openDetailPage(reminder: Reminder) {
        view.showDetailPage(reminder)
    }

    override fun start() {
        isRunning = true
        checksUserRole()
        loadReminderList()
    }
}