package um.informatika.freelancer.reminddoc.feature.reminder.add

import um.informatika.freelancer.reminddoc.base.BaseCallback
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.manager.ReminderDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder

class AddReminderPresenter(
    private val view: AddReminderContract.View,
    private val reminderManager: ReminderDataManager,
    private val accountManager: AccountDataManager
) : AddReminderContract.Presenter {

    private val dokterMap = hashMapOf<String, String>()

    override fun loadDokterList() {
        view.showLoadingIndicator(true)
        reminderManager.loadDokterList(
            object : BaseCallback<Map<String, String>> {
                override fun onSuccess(data: Map<String, String>) {
                    dokterMap.clear()
                    dokterMap.putAll(data)
                    view.showLoadingIndicator(false)
                    view.onLoadDokterListSuccess(data.values)
                }

                override fun onError(message: String) {
                    view.showLoadingIndicator(false)
                    view.onLoadDokterListError(message)
                }
            }
        )
    }

    override fun saveReminder(reminder: Reminder) {
        if (!dokterMap.containsValue(reminder.namaDokter)) {
            view.onSaveReminderError("Kolom dokter tujuan tidak boleh kosong.")
            return
        }
        view.showLoadingIndicator(true)
        for (key in dokterMap.keys) {
            if (reminder.namaDokter == dokterMap[key]) {
                reminder.uidDokter = key
                break
            }
        }
        reminder.uidPetugas = accountManager.uid
        reminder.namaPetugas = accountManager.name
        reminderManager.insertReminder(
            reminder,
            object : BaseCallback<Unit> {
                override fun onSuccess(data: Unit) {
                    view.showLoadingIndicator(false)
                    view.onSaveReminderSuccess()
                }

                override fun onError(message: String) {
                    view.showLoadingIndicator(false)
                    view.onSaveReminderError(message)
                }
            }
        )
    }

    override fun updateReminder(reminder: Reminder) {
        if (!dokterMap.containsValue(reminder.namaDokter)) {
            view.onSaveReminderError("Kolom dokter tujuan tidak boleh kosong.")
            return
        }
        view.showLoadingIndicator(true)
        for (key in dokterMap.keys) {
            if (reminder.namaDokter == dokterMap[key]) {
                reminder.uidDokter = key
                break
            }
        }
        reminder.uidPetugas = accountManager.uid
        reminder.namaPetugas = accountManager.name
        reminderManager.updateReminder(
            reminder,
            object : BaseCallback<Unit> {
                override fun onSuccess(data: Unit) {
                    view.showLoadingIndicator(false)
                    view.onSaveReminderSuccess()
                }

                override fun onError(message: String) {
                    view.showLoadingIndicator(false)
                    view.onSaveReminderError(message)
                }
            }
        )
    }

    override fun start() = loadDokterList()
}