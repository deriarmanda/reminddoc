package um.informatika.freelancer.reminddoc.feature.report


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.fragment_report.*

import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.manager.ReminderDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.feature.reminder.detail.DetailReminderActivity
import um.informatika.freelancer.reminddoc.util.showErrorMessage

/**
 * A simple [Fragment] subclass.
 *
 */
class ReportFragment : Fragment(), ReportContract.View {

    override lateinit var presenter: ReportContract.Presenter

    private lateinit var keysAdapter: ArrayAdapter<String>
    private val onItemClickListener =
        object : ReportRvAdapter.OnItemClickListener {
            override fun onClick(reminder: Reminder) {
                presenter.openDetailPage(reminder)
            }
        }
    private val listAdapter = ReportRvAdapter(onItemClickListener)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_report, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        keysAdapter = ArrayAdapter(context!!, android.R.layout.simple_spinner_dropdown_item)
        spinner_times.adapter = keysAdapter
        spinner_types.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {}
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) presenter.loadAllLaporanHarian()
                else if (position == 1) presenter.loadAllLaporanBulanan()
            }
        }

        button_find.setOnClickListener {
            val type = spinner_types.selectedItemPosition
            val key = keysAdapter.getItem(spinner_times.selectedItemPosition)!!

            if (type == 0) presenter.loadSelectedLaporanHarian(key)
            else if (type == 1) presenter.loadSelectedLaporanBulanan(key)
        }

        with(list_report) {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }

        text_total.text = getString(R.string.report_msg_total, 0)

        presenter = ReportPresenter(
            this,
            AccountDataManager.getInstance(),
            ReminderDataManager.getInstance()
        )
        presenter.start()
    }

    override fun onPause() {
        super.onPause()
        presenter.breakOperations()
    }

    override fun showLaporanKeyList(list: List<String>) {
        keysAdapter.clear()
        if (list.isEmpty()) keysAdapter.add("-")
        else keysAdapter.addAll(list)
        keysAdapter.notifyDataSetChanged()
    }

    override fun showSelectedLaporanList(list: List<Reminder>) {
        text_total.text = getString(R.string.report_msg_total, list.size)
        listAdapter.list = list

        list_report.visibility = if (list.isEmpty()) View.GONE else View.VISIBLE
        text_empty.visibility = if (list.isEmpty()) View.VISIBLE else View.GONE
    }

    override fun onLoadAllLaporanFailed(message: String) {
        (activity as AppCompatActivity).showErrorMessage(message)
    }

    override fun showDetailPage(reminder: Reminder) {
        startActivity(DetailReminderActivity.getIntent(context!!, reminder))
    }

    override fun showLoadingIndicator(isActive: Boolean) {
        progress_bar.visibility = if (isActive) View.VISIBLE else View.GONE
    }

}
