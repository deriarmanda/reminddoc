package um.informatika.freelancer.reminddoc.feature.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.feature.home.HomeFragment
import um.informatika.freelancer.reminddoc.feature.login.LoginActivity
import um.informatika.freelancer.reminddoc.feature.rating.RatingFragment
import um.informatika.freelancer.reminddoc.feature.reminder.ReminderFragment
import um.informatika.freelancer.reminddoc.feature.reminder.detail.DetailReminderActivity
import um.informatika.freelancer.reminddoc.feature.report.ReportFragment
import um.informatika.freelancer.reminddoc.util.*

class MainActivity : AppCompatActivity(), HomeFragment.Callback {

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            val fragment: Fragment?
            when (item.itemId) {
                R.id.nav_home -> {
                    fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_HOME) ?: HomeFragment()
                    replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_TAG_HOME)
                    toolbar.visibility = View.GONE
                    search_view.visibility = View.GONE
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_reminder -> {
                    fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_REMINDER) ?: ReminderFragment()
                    replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_TAG_REMINDER)
                    toolbar.setTitle(R.string.title_reminder)
                    toolbar.subtitle = accountManager.name
                    toolbar.visibility = View.VISIBLE
                    search_view.visibility = View.VISIBLE
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_report -> {
                    fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_REPORT) ?: ReportFragment()
                    replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_TAG_REPORT)
                    toolbar.setTitle(R.string.title_report)
                    toolbar.setSubtitle(R.string.subtitle_report)
                    toolbar.visibility = View.VISIBLE
                    search_view.visibility = View.GONE
                    return@OnNavigationItemSelectedListener true
                }
                R.id.nav_rating -> {
                    fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_RATING) ?: RatingFragment()
                    replaceFragmentByTag(R.id.fragment_container, fragment, FRAGMENT_TAG_RATING)
                    toolbar.setTitle(R.string.title_rating)
                    toolbar.setSubtitle(R.string.subtitle_rating)
                    toolbar.visibility = View.VISIBLE
                    search_view.visibility = View.GONE
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }
    private val accountManager = AccountDataManager.getInstance()

    companion object {
        private const val EXTRA_NOTIF_REMINDER = "extra_notif_reminder"
        fun getIntent(context: Context, reminder: Reminder? = null): Intent {
            val intent = Intent(context, MainActivity::class.java)
            if (reminder != null) intent.putExtra(EXTRA_NOTIF_REMINDER, reminder)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.nav_home

        search_view.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    val fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_REMINDER)
                    return if (fragment != null || navigation.selectedItemId != R.id.nav_reminder) {
                        (fragment as ReminderFragment).searchList(query ?: "")
                        true
                    } else false
                }

                override fun onQueryTextChange(query: String?): Boolean {
                    val fragment = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG_REMINDER)
                    return if (fragment != null || navigation.selectedItemId != R.id.nav_reminder || query.isNullOrBlank()) {
                        (fragment as ReminderFragment).searchList("")
                        true
                    } else false
                }
            }
        )

        if (intent.hasExtra(EXTRA_NOTIF_REMINDER)) {
            val reminder = intent.getParcelableExtra<Reminder>(EXTRA_NOTIF_REMINDER)
            startActivity(DetailReminderActivity.getIntent(this, reminder))
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == R.id.menu_logout) {
            accountManager.doLogout()
            showInfoMessage("Sampai jumpa kembali !")
            startActivity(LoginActivity.getIntent(this))
            finish()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun showReminderPage() {
        navigation.selectedItemId = R.id.nav_reminder
    }
}
