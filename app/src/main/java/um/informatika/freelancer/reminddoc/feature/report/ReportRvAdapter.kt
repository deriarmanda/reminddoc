package um.informatika.freelancer.reminddoc.feature.report

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_report.view.*
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.util.DateTimeUtils

class ReportRvAdapter(
    private val onItemClickListener: OnItemClickListener
) : RecyclerView.Adapter<ReportRvAdapter.ViewHolder>() {

    var list = emptyList<Reminder>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_report, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = list.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.fetch(list[position])

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(reminder: Reminder) {
            itemView.text_tanggal.text = DateTimeUtils.getHumanDateTimeString(reminder.tanggal)
            itemView.text_dokter.text = reminder.namaDokter
            itemView.text_pasien.text = String.format("%s / %s", reminder.nomorRM, reminder.namaPasien)
            itemView.text_keterangan.text = reminder.keterangan
            itemView.text_status.text = reminder.status
            itemView.text_status.setTextColor(
                if (reminder.status == "Belum Lengkap") Color.parseColor("#F44336")
                else Color.parseColor("#8BC34A")
            )
            itemView.root.setOnClickListener { onItemClickListener.onClick(reminder) }
        }
    }

    interface OnItemClickListener {
        fun onClick(reminder: Reminder)
    }
}