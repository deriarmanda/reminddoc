package um.informatika.freelancer.reminddoc.feature.notification

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.app.NotificationCompat
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.feature.reminder.add.AddReminderActivity

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        Log.d("MyFBMessagingService", "New token: $token")
        if (token != null) sendTokenToServer(token)
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        super.onMessageReceived(message)
        Log.d("MyFBMessagingService", "onMessageReceived")

        message?.data?.isNotEmpty()?.let {
            Log.d("MyFBMessagingService", "Data: ${message.data["namaDokter"]}")
        }

    }

    private fun sendTokenToServer(token: String) {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            val dbRef = FirebaseDatabase.getInstance().getReference("/users")
            dbRef.child(user.uid).child("token").setValue(token).addOnCompleteListener {
                Log.d("MyFBMessagingService", "sendTokenToServer results: ${it.result}")
            }
        }
    }

    private fun buildNotificationsForPetugas() {
        val targetIntent = AddReminderActivity.getIntent(this)
        targetIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP

        val pendingIntent = PendingIntent.getActivity(
            this,
            100,
            targetIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val notification = NotificationCompat.Builder(this, "um.informatika.freelancer.reminddoc")
            .setContentIntent(pendingIntent)
            .setSmallIcon(R.drawable.logo_app_144dp)
            .setColorized(true)
            .setColor(Color.parseColor("#2196F3"))
            .setContentTitle(getString(R.string.app_name))
            .setContentText("Text")
            .setAutoCancel(true)
            .setOngoing(true)
            .setDefaults(Notification.DEFAULT_SOUND)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .build()

        val notifManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notifManager.notify(100, notification)
    }

    private fun buildNotificationForDokter() {

    }
}