package um.informatika.freelancer.reminddoc.base

interface BaseCallback<T> {
    fun onSuccess(data: T)
    fun onError(message: String)
}