package um.informatika.freelancer.reminddoc.feature.rating


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_rating.*

import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.base.BaseCallback
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.util.showErrorMessage
import um.informatika.freelancer.reminddoc.util.showSuccessMessage

/**
 * A simple [Fragment] subclass.
 *
 */
class RatingFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rating, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val accountManager = AccountDataManager.getInstance()

        rating_bar.rating = accountManager.rate.toFloat()
        button_send_rate.setOnClickListener {
            progress_bar.visibility = View.VISIBLE
            accountManager.sendUserRating(
                rating_bar.rating.toInt(),
                object : BaseCallback<Unit> {
                    override fun onSuccess(data: Unit) {
                        progress_bar.visibility = View.GONE
                        (activity as AppCompatActivity).showSuccessMessage("Rating anda telah terkirim, terima kasih atas kontribusi anda.")
                    }

                    override fun onError(message: String) {
                        progress_bar.visibility = View.GONE
                        (activity as AppCompatActivity).showErrorMessage("Gagal mengirim rating, periksa koneksi internet anda.")
                    }
                }
            )
        }
    }
}
