package um.informatika.freelancer.reminddoc.feature.reminder.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_detail_reminder.*
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.base.BaseCallback
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.manager.ReminderDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.feature.reminder.add.AddReminderActivity
import um.informatika.freelancer.reminddoc.util.DateTimeUtils
import um.informatika.freelancer.reminddoc.util.showErrorMessage
import um.informatika.freelancer.reminddoc.util.showSuccessMessage

class DetailReminderActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_REMINDER = "extra_reminder"
        fun getIntent(context: Context, reminder: Reminder): Intent {
            val intent = Intent(context, DetailReminderActivity::class.java)
            intent.putExtra(EXTRA_REMINDER, reminder)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_reminder)

        val reminder = intent.getParcelableExtra<Reminder>(EXTRA_REMINDER)
        text_dear.text = getString(R.string.reminder_msg_dear_doc, reminder.namaDokter)
        text_tanggal.text = DateTimeUtils.getHumanDateString(reminder.tanggal)
        text_pukul.text = DateTimeUtils.getHumanTimeString(reminder.tanggal)
        text_no_rm.text = reminder.nomorRM
        text_nama_pasien.text = reminder.namaPasien
        text_petugas.text = reminder.namaPetugas
        val keteranganAll = reminder.keterangan.split(", ")
        var keterangan = getString(R.string.symbol_dot) + "  ${keteranganAll[0]}"
        for (index in 1 until keteranganAll.size) {
            keterangan += ("\n" + getString(R.string.symbol_dot) + "  ${keteranganAll[index]}")
        }
        text_keterangan.text = keterangan

        button_edit.setOnClickListener {
            startActivity(AddReminderActivity.getIntent(this, reminder))
            finish()
        }
        val accountManager = AccountDataManager.getInstance()
        button_edit.visibility = if (accountManager.role == "petugas") View.VISIBLE else View.GONE

        val reminderManager = ReminderDataManager.getInstance()
        button_done.isEnabled = reminder.status == "Belum Lengkap"
        button_done.setOnClickListener {
            button_done.isEnabled = false
            reminder.status = "Lengkap"
            reminderManager.updateStatusReminder(
                reminder,
                object : BaseCallback<Unit> {
                    override fun onSuccess(data: Unit) {
                        button_done.isEnabled = true
                        this@DetailReminderActivity.showSuccessMessage("Berhasil ditandai sebagai dokumen yang lengkap.")
                        finish()
                    }

                    override fun onError(message: String) {
                        this@DetailReminderActivity.showErrorMessage(message)
                        button_done.isEnabled = true
                    }
                }
            )
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }
}
