package um.informatika.freelancer.reminddoc.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.firebase.database.Exclude
import um.informatika.freelancer.reminddoc.util.DateTimeUtils
import java.util.*

data class Reminder (
    var tanggal: String = DateTimeUtils.formatSortableDate(Date()),
    var nomorRM: String = "",
    var namaPasien: String = "",
    var keterangan: String = "-",
    var status: String = "Belum Lengkap",
    var uidDokter: String = "",
    var uidPetugas: String = "",
    @get:Exclude var uid: String = "",
    @get:Exclude var namaPetugas: String = "",
    @get:Exclude var namaDokter: String = "Dokter"
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "-",
        parcel.readString() ?: "Belum Lengkap",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "Dokter"
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(tanggal)
        parcel.writeString(nomorRM)
        parcel.writeString(namaPasien)
        parcel.writeString(keterangan)
        parcel.writeString(status)
        parcel.writeString(uidDokter)
        parcel.writeString(uidPetugas)
        parcel.writeString(uid)
        parcel.writeString(namaPetugas)
        parcel.writeString(namaDokter)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Reminder> {
        override fun createFromParcel(parcel: Parcel): Reminder {
            return Reminder(parcel)
        }

        override fun newArray(size: Int): Array<Reminder?> {
            return arrayOfNulls(size)
        }
    }
}