package um.informatika.freelancer.reminddoc.util

import java.text.SimpleDateFormat
import java.util.*

object DateTimeUtils {

    private const val HUMAN_DAY_DATE_PATTERN = "EEEE, d MMMM yyyy"
    private const val HUMAN_MONTH_YEAR_DATE_PATTERN = "MMMM yyyy"
    private const val HUMAN_DATE_TIME_PATTERN = "d MMMM yyyy / HH:mm"
    private const val HUMAN_DATE_PATTERN = "d MMMM yyyy"
    private const val DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm"
    private const val TIME_PATTERN = "HH.mm"
    private val HUMAN_DAY_DATE_FORMATTER = SimpleDateFormat(HUMAN_DAY_DATE_PATTERN, Locale.ENGLISH)
    private val HUMAN_MONTH_YEAR_FORMATTER = SimpleDateFormat(HUMAN_MONTH_YEAR_DATE_PATTERN, Locale.ENGLISH)
    private val HUMAN_DATE_TIME_FORMATTER = SimpleDateFormat(HUMAN_DATE_TIME_PATTERN, Locale.ENGLISH)
    private val HUMAN_DATE_FORMATTER = SimpleDateFormat(HUMAN_DATE_PATTERN, Locale.ENGLISH)
    private val DATE_TIME_FORMATTER = SimpleDateFormat(DATE_TIME_PATTERN, Locale.ENGLISH)
    private val TIME_FORMATTER = SimpleDateFormat(TIME_PATTERN, Locale.ENGLISH)

    @JvmStatic
    fun getHumanDateTimeString(dateString: String): String {
        val date = parseSortableDate(dateString)
        return HUMAN_DATE_TIME_FORMATTER.format(date)
    }

    @JvmStatic
    fun getHumanDateString(dateString: String): String {
        val date = parseSortableDate(dateString)
        return HUMAN_DATE_FORMATTER.format(date)
    }

    @JvmStatic
    fun getHumanTimeString(dateString: String): String {
        val date = parseSortableDate(dateString)
        return TIME_FORMATTER.format(date)
    }

    @JvmStatic
    fun getHumanMonthYearOnly(dateString: String): String {
        val date = parseSortableDate(dateString)
        return HUMAN_MONTH_YEAR_FORMATTER.format(date)
    }

    @JvmStatic
    fun formatHumanDayDate(date: Date) = HUMAN_DAY_DATE_FORMATTER.format(date)!!

    @JvmStatic
    fun formatHumanDateTime(date: Date) = HUMAN_DATE_TIME_FORMATTER.format(date)!!

    @JvmStatic
    fun formatHumanDate(date: Date) = HUMAN_DATE_FORMATTER.format(date)!!

    @JvmStatic
    fun formatSortableDate(date: Date) = DATE_TIME_FORMATTER.format(date)!!

    @JvmStatic
    fun formatSortableTime(time: Date) = TIME_FORMATTER.format(time)!!

    private fun parseSortableDate(dateString: String) = DATE_TIME_FORMATTER.parse(dateString)
}