package um.informatika.freelancer.reminddoc.feature.home


import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_home.view.*

import um.informatika.freelancer.reminddoc.R

/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : Fragment() {

    private lateinit var callback: Callback

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is Callback) callback = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.button_show_reminder.setOnClickListener { callback.showReminderPage() }
    }

    interface Callback {
        fun showReminderPage()
    }
}
