package um.informatika.freelancer.reminddoc.feature.reminder

import um.informatika.freelancer.reminddoc.base.BasePresenter
import um.informatika.freelancer.reminddoc.base.BaseView
import um.informatika.freelancer.reminddoc.data.model.Reminder

interface ReminderContract {

    interface Presenter : BasePresenter {
        var isRunning: Boolean
        fun breakOperations()
        fun checksUserRole()
        fun loadReminderList()
        fun sortList(sortBy: String)
        fun searchList(query: String)
        fun openDetailPage(reminder: Reminder)
    }

    interface View : BaseView<Presenter> {
        fun showAddButton()
        fun hideAddButton()
        fun showReminderList(list: List<Reminder>)
        fun showEmptyListMessage()
        fun onLoadReminderListFailed(message: String)
        fun showDetailPage(reminder: Reminder)
    }
}