package um.informatika.freelancer.reminddoc.feature.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_login.*
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.feature.main.MainActivity
import um.informatika.freelancer.reminddoc.util.showErrorMessage
import um.informatika.freelancer.reminddoc.util.showInfoMessage

class LoginActivity : AppCompatActivity(), LoginContract.View {

    override lateinit var presenter: LoginContract.Presenter

    companion object {
        fun getIntent(context: Context) = Intent(context, LoginActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        presenter = LoginPresenter(this, AccountDataManager.getInstance())

        button_login.setOnClickListener {
            presenter.loginToFirebase(
                field_email.text.toString(),
                field_password.text.toString()
            )
        }
        field_password.clearFocus()
        field_email.requestFocus()
    }

    override fun onLoginSuccess(name: String) {
        showInfoMessage("Selamat datang kembali, $name")
        startActivity(MainActivity.getIntent(this))
        finish()
    }

    override fun onLoginError(message: String) {
        showErrorMessage(message)
    }

    override fun showLoadingIndicator(isActive: Boolean) {
        progress_bar.visibility = if (isActive) View.VISIBLE else View.GONE
        button_login.isEnabled = !isActive
    }

}
