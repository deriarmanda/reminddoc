package um.informatika.freelancer.reminddoc.feature.report

import um.informatika.freelancer.reminddoc.base.BaseCallback
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.manager.ReminderDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder

class ReportPresenter(
    private val view: ReportContract.View,
    private val accountManager: AccountDataManager,
    private val reminderManager: ReminderDataManager
) : ReportContract.Presenter {

    override lateinit var laporanBulanan: Map<String, List<Reminder>>
    override lateinit var laporanHarian: Map<String, List<Reminder>>
    private var isRunning = false

    override fun breakOperations() {
        isRunning = false
    }

    override fun loadAllLaporanBulanan() {
        if (laporanBulanan.isNotEmpty()) {
            view.showLaporanKeyList(laporanBulanan.keys.toList())
            return
        }

        view.showLoadingIndicator(true)
        reminderManager.loadLaporanBulananList(
            accountManager.uid,
            accountManager.role,
            object : BaseCallback<Map<String, List<Reminder>>> {
                override fun onSuccess(data: Map<String, List<Reminder>>) {
                    laporanBulanan = data
                    if (isRunning) {
                        view.showLoadingIndicator(false)
                        view.showLaporanKeyList(laporanBulanan.keys.toList())
                    }
                }

                override fun onError(message: String) {
                    if (isRunning) {
                        view.showLoadingIndicator(false)
                        view.onLoadAllLaporanFailed(message)
                    }
                }
            }
        )
    }

    override fun loadAllLaporanHarian() {
        if (laporanHarian.isNotEmpty()) {
            view.showLaporanKeyList(laporanHarian.keys.toList())
            return
        }

        view.showLoadingIndicator(true)
        reminderManager.loadLaporanHarianList(
            accountManager.uid,
            accountManager.role,
            object : BaseCallback<Map<String, List<Reminder>>> {
                override fun onSuccess(data: Map<String, List<Reminder>>) {
                    laporanHarian = data
                    if (isRunning) {
                        view.showLoadingIndicator(false)
                        view.showLaporanKeyList(laporanHarian.keys.toList())
                    }
                }

                override fun onError(message: String) {
                    if (isRunning) {
                        view.showLoadingIndicator(false)
                        view.onLoadAllLaporanFailed(message)
                    }
                }
            }
        )
    }

    override fun loadSelectedLaporanBulanan(bulan: String) {
        if (laporanBulanan.contains(bulan)) view.showSelectedLaporanList(laporanBulanan[bulan]!!)
        else view.showSelectedLaporanList(emptyList())
    }

    override fun loadSelectedLaporanHarian(hari: String) {
        if (laporanHarian.contains(hari)) view.showSelectedLaporanList(laporanHarian[hari]!!)
        else view.showSelectedLaporanList(emptyList())
    }

    override fun openDetailPage(reminder: Reminder) {
        view.showDetailPage(reminder)
    }

    override fun start() {
        isRunning = true
        laporanBulanan = emptyMap()
        laporanHarian = emptyMap()

        loadAllLaporanBulanan()
        loadAllLaporanHarian()
    }
}