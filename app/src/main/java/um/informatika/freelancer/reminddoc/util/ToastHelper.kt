package um.informatika.freelancer.reminddoc.util

import android.support.v7.app.AppCompatActivity
import android.view.Gravity
import android.widget.Toast
import es.dmoral.toasty.Toasty

fun AppCompatActivity.showInfoMessage(message: String) {
    val toast = Toasty.info(this, message)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}

fun AppCompatActivity.showSuccessMessage(message: String) {
    val toast = Toasty.success(this, message)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}

fun AppCompatActivity.showErrorMessage(message: String) {
    val toast = Toasty.error(this, message, Toast.LENGTH_LONG)
    toast.setGravity(Gravity.CENTER, 0, 0)
    toast.show()
}