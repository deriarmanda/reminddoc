package um.informatika.freelancer.reminddoc.feature.reminder.add

import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.activity_add_reminder.*
import um.informatika.freelancer.reminddoc.R
import um.informatika.freelancer.reminddoc.data.manager.AccountDataManager
import um.informatika.freelancer.reminddoc.data.manager.ReminderDataManager
import um.informatika.freelancer.reminddoc.data.model.Reminder
import um.informatika.freelancer.reminddoc.util.DateTimeUtils
import um.informatika.freelancer.reminddoc.util.showErrorMessage
import um.informatika.freelancer.reminddoc.util.showSuccessMessage
import java.util.*

class AddReminderActivity : AppCompatActivity(), AddReminderContract.View {

    private val listKeterangan = arrayOf(
        "Identitas Pasien",
        "Jam Keluar Masuk RS",
        "Alasan Masuk RS",
        "Diagnosa Masuk",
        "Pemeriksaan Fisik & Penunjang",
        "Diagnosa Akhir",
        "Tindakan",
        "Pengobatan",
        "Tindak Lanjut",
        "Nama dan TTD DPJP"
    )
    private val checkedListKeterangan = BooleanArray(10)

    override lateinit var presenter: AddReminderContract.Presenter
    private lateinit var datePickerDialog: DatePickerDialog
    private lateinit var choicesDialogBuilder: AlertDialog.Builder
    private lateinit var spinnerAdapter: ArrayAdapter<String>
    private lateinit var reminder: Reminder

    companion object {
        private const val EXTRA_REMINDER = "extra_reminder"
        fun getIntent(context: Context, reminder: Reminder = Reminder()): Intent {
            val intent = Intent(context, AddReminderActivity::class.java)
            intent.putExtra(EXTRA_REMINDER, reminder)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_reminder)

        reminder = intent.getParcelableExtra(EXTRA_REMINDER)

        val calendar = Calendar.getInstance()
        datePickerDialog = DatePickerDialog(
            this,
            R.style.DatePickerStyle,
            DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                val selectedCalender = Calendar.getInstance()
                selectedCalender.set(Calendar.YEAR, year)
                selectedCalender.set(Calendar.MONTH, month)
                selectedCalender.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val date = selectedCalender.time

                reminder.tanggal = DateTimeUtils.formatSortableDate(date)
                text_tanggal.text = DateTimeUtils.formatHumanDate(date)
            },
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        choicesDialogBuilder = AlertDialog.Builder(this)
        choicesDialogBuilder.setCancelable(false)
        choicesDialogBuilder.setTitle(R.string.reminder_hint_keterangan_dialog)
        choicesDialogBuilder.setPositiveButton(R.string.action_save) { _, _ ->
            var keterangan = ""
            for (index in 0 until checkedListKeterangan.size) {
                if (checkedListKeterangan[index]) keterangan += "${listKeterangan[index]}, "
            }
            keterangan =
                    if (keterangan.length > 2) keterangan.substring(0, keterangan.length - 2)
                    else "-"

            text_keterangan.text = keterangan
            reminder.keterangan = keterangan
        }

        spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item)
        spinner_dokter.adapter = spinnerAdapter

        button_tanggal.setOnClickListener { datePickerDialog.show() }
        button_edit.setOnClickListener {
            choicesDialogBuilder.setMultiChoiceItems(listKeterangan, checkedListKeterangan) { _, index, isChecked ->
                checkedListKeterangan[index] = isChecked
            }
            choicesDialogBuilder.create().show()
        }
        button_simpan.setOnClickListener {
            reminder.status = "Belum Lengkap"
            reminder.nomorRM = field_no_rm.text?.toString() ?: ""
            reminder.namaPasien = field_nama_pasien.text?.toString() ?: ""
            reminder.namaDokter = spinner_dokter.selectedItem?.toString() ?: "Dokter"

            if (reminder.uid.isEmpty()) presenter.saveReminder(reminder)
            else presenter.updateReminder(reminder)
        }

        presenter = AddReminderPresenter(
            this,
            ReminderDataManager.getInstance(),
            AccountDataManager.getInstance()
        )
        presenter.start()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    override fun onLoadDokterListSuccess(list: Collection<String>) {
        spinnerAdapter.clear()
        spinnerAdapter.addAll(list)
        spinnerAdapter.notifyDataSetChanged()
        fetchData()
    }

    override fun onLoadDokterListError(message: String) {
        showErrorMessage(message)
    }

    override fun onSaveReminderSuccess() {
        showSuccessMessage("Berhasil menambahkan daftar pengingat.")
        finish()
    }

    override fun onSaveReminderError(message: String) {
        showErrorMessage(message)
    }

    override fun showLoadingIndicator(isActive: Boolean) {
        progress_bar.visibility = if (isActive) View.VISIBLE else View.GONE
    }

    private fun fetchData() {
        text_tanggal.text = DateTimeUtils.formatHumanDate(Date())
        Arrays.fill(checkedListKeterangan, false)
        if (reminder.uid.isNotEmpty()) {
            field_no_rm.setText(reminder.nomorRM)
            field_nama_pasien.setText(reminder.namaPasien)
            text_tanggal.text = DateTimeUtils.getHumanDateString(reminder.tanggal)
            spinner_dokter.setSelection(spinnerAdapter.getPosition(reminder.namaDokter))
            text_keterangan.text = reminder.keterangan

            val keteranganAll = reminder.keterangan.split(", ")
            for (ket in keteranganAll) {
                for (index in 0 until listKeterangan.size) {
                    if (listKeterangan[index] == ket) {
                        checkedListKeterangan[index] = true
                        break
                    }
                }
            }
        }
    }
}
